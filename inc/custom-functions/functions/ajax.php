<?php

function display_image_upload_form() {
    ob_start();
    $object_type = get_terms( array( 
        'taxonomy' => 'real-estate-type', 
        'hide_empty' => false,
    ) );

    $args = array(  
        'post_type' => 'city',
        'post_status' => 'publish',
        'posts_per_page' => -1, 
        'orderby' => 'title', 
        'order' => 'ASC', 
    );
    $cities = new WP_Query( $args );
    ?>
    <section class="property-form">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4>Добавить новый объект недвижимость:</h4>
                    <form id="image-upload-form" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group pb-4">
                            <label for="property-name">Заголовок</label>
                            <input type="text" class="form-control" id="property-name" placeholder="" >
                        </div>
                        <div class="form-group pb-4">
                            <label for="property-city">Город </label>
                            <select class="form-control" id="property-city">
                                <?php 
                                    while ( $cities->have_posts() ) : $cities->the_post(); 
                                        echo '<option value="'.get_the_ID().'">'.get_the_title().'</option>';
                                    endwhile;
                                    wp_reset_postdata();
                                ?>
                            </select> 
                        </div>  
                        <div class="form-group pb-4">
                            <label for="property-type">Тип объекта  </label>
                            <select class="form-control" id="property-type">
                                <?php if(!empty($object_type)):
                                    foreach($object_type as $type){ 
                                        echo '<option value="'.$type->term_id.'">'.$type->name.'</option>';
                                    }
                                endif;?>
                            </select> 
                        </div>                                                                     
                        <div class="form-group pb-4">
                            <label for="property-description">Описание </label>
                            <textarea class="form-control" id="property-description" rows="3"></textarea>
                        </div>                            
                        <div class="form-group pb-4">
                            <label for="property-square">Площадь</label>
                            <input type="text" class="form-control" id="property-square" placeholder="" >
                        </div>         
                        <div class="form-group pb-4">
                            <label for="property-price">Стоимость</label>
                            <input type="text" class="form-control" id="property-price" placeholder="" >
                        </div>       
                        <div class="form-group pb-4">
                            <label for="property-address">Адрес</label>
                            <input type="text" class="form-control" id="property-address" placeholder="" >
                        </div>                       
                        <div class="form-group pb-4">
                            <label for="property-living-space">Жилая площадь</label>
                            <input type="text" class="form-control" id="property-living-space" placeholder="" >
                        </div>        
                        <div class="form-group pb-4">
                            <label for="property-floor">Этаж</label>
                            <input type="text" class="form-control" id="property-floor" placeholder="" >
                        </div>      
                        <div class="form-group pb-4">
                            <label for="images">Фото объекта</label>
                            <input type="file" class="form-control-file" name="images[]" id="images" multiple>
                        </div>                                                                                                                                    
                       
                        <input type="submit" class="btn btn-primary" name="submit" value="Добавить объект">
                    </form>
                    <div id="upload-status"></div>
                </div>
            </div>
        </div>
    </section>
<?php
    return ob_get_clean();
}
add_shortcode('image_upload_form', 'display_image_upload_form');



add_action('wp_ajax_upload_images', 'ajax_upload_images');
add_action('wp_ajax_nopriv_upload_images', 'ajax_upload_images');

function ajax_upload_images() {
    $parent_post_id = isset( $_POST['post_id'] ) ? $_POST['post_id'] : 0; 
    $valid_formats = array("jpg", "png", "jpeg"); 
    $max_image_upload = 500; 
    $max_file_size = 1024 * 500; 
    $wp_upload_dir = wp_upload_dir();
    $path = $wp_upload_dir['path'] . '/';
    $count = 0;

    $images_url = array(); 
   

   
    $attachments = get_posts( array(
        'post_type'         => 'attachment',
        'posts_per_page'    => -1,
        'post_parent'       => $parent_post_id,
        'exclude'           => get_post_thumbnail_id()  
    ) );
   
    if( $_SERVER['REQUEST_METHOD'] == "POST" ){
        if( ( count( $attachments ) + count( $_FILES['images']['name'] ) ) > $max_image_upload ) {
        
        }else{
            
            foreach ( $_FILES['images']['name'] as $f => $name ) {
                $extension = pathinfo( $name, PATHINFO_EXTENSION );
                $new_filename = $name  . '.' . $extension;
                  
                if ( $_FILES['images']['error'][$f] > 0 ) {
                     
                    continue;
                }elseif( ! in_array( strtolower( $extension ), $valid_formats ) ){
                    
                    $upload_message[] = "$name is not a valid format";
                    continue; 
                }else{
                    if( move_uploaded_file( $_FILES["images"]["tmp_name"][$f], $path.$new_filename ) ) { 
                        $count++;

                        $filename = $path.$new_filename;
                        $filetype = wp_check_filetype( basename( $filename ), null );
                        $wp_upload_dir = wp_upload_dir();
                        $attachment = array(
                            'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
                            'post_mime_type' => $filetype['type'],
                            'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                            'post_content'   => '',
                            'post_status'    => 'inherit'
                        );
                        
                        $attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id ); 
                        require_once( ABSPATH . 'wp-admin/includes/image.php' ); 
                        $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                         
                        wp_update_attachment_metadata( $attach_id, $attach_data );
                       $imageUrl = wp_get_attachment_image_url($attach_id);
                       $images_url[] = $attach_id;
                       
                    }
                }
            }
        }
    } 
    $images_url_string = implode(',', $images_url);
    $data = array(
        'name' => $_POST['property-name'],
        'description' => $_POST['property-description'],
        'square' => $_POST['property-square'],
        'price' => $_POST['property-price'],
        'address' => $_POST['property-address'],
        'living_space' => $_POST['property-living-space'],
        'floor' => $_POST['property-floor'], 
        'images' =>  $images_url_string,
        'city' => $_POST['property-city'], 
        'type' => $_POST['property-type'], 
    );
    
    $crate_property = create_new_property($data);
    
    $return = array(
        'message' => 'Произошла ошибка, попробуйте еще раз.',
        'status' => false
    );
    
    if(true === $crate_property){
        $return = array(
            'message' => 'Объект создан успешно. ',
            'status' => true
        );
    }

    
    echo wp_send_json($return);
    wp_die();
}





function create_new_property($data){
    
    
    if( $data){
        

        $new_post = array(
            'ID' => '',
            'post_type' => 'real-estate',
            'post_status' => 'draft',
            'post_title' => $data['name'], 
            'post_content' => $data['description'] 
        );
    
        $post_id = wp_insert_post($new_post);
        wp_set_post_terms( $post_id, $data['type'], 'real-estate-type' ); 
        update_post_meta($post_id, '_real_estate_city', sanitize_text_field($data['city']));
        update_field('square', $data['square'], $post_id);
        update_field('price', $data['price'], $post_id);
        update_field('address', $data['address'], $post_id);
        update_field('living_space', $data['living_space'], $post_id);
        update_field('floor', $data['floor'], $post_id);
        update_field('object_gallery', $data['images'] , $post_id);

        if($post_id){
            return true;
        }        
    }

    return false;
}