<?php 

function enque_style_and_scripts(){
	wp_enqueue_style( 'slick-slider-css', '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css', array(), null );
	wp_enqueue_style( 'slick-slider-theme', '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css', array(), null );
    wp_enqueue_script( 'slick-slider-js', '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js', array(), null, true );
    wp_localize_script('child-understrap-scripts-js', 'ajaxurl', admin_url('admin-ajax.php'));
}

add_action('wp_enqueue_scripts','enque_style_and_scripts');

 