<?php
/**
 * Post types registration
 */
abstract class RegisterNewPostType{
    protected $labels;
    protected $taxonomy_labels;

    public function __construct($labels, $taxonomy_labels){
        $this->labels = $labels;
        $this->taxonomy_labels = $taxonomy_labels;
        add_action('init', array($this,'register_post_type'));
        if ($this->get_taxonomy_name() && method_exists($this, 'register_taxonomy')) {
            add_action('init', array($this, 'register_taxonomy')); 
        }
    }
    
    abstract protected function get_post_type_name();
    abstract protected function get_taxonomy_name();
    abstract protected function get_additional_args();
    abstract protected function get_additional_taxonomy_args();

    
    protected function get_default_args(){
        return array(
            'labels' => $this->labels,
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => 'custom-post-type'),
            'menu_position' => 5,  
            'supports'      => array('title', 'editor', 'thumbnail', 'custom-fields'),
            'menu_icon'     => 'dashicons-building', 
        );
    }
    
    protected function get_default_taxonomy_args(){
        return array(
            'labels' => $this->taxonomy_labels,
            'rewrite' => array('slug' => $this->get_taxonomy_name()),
            'hierarchical' => true,
            'show_in_rest' => true
        );
    }



    public function register_post_type() {
        $args = array_merge(
            $this->get_default_args(),  
            $this->get_additional_args()
        );
        register_post_type($this->get_post_type_name(), $args); 
    }
    

    public function register_taxonomy(){
        $args = array_merge(
            $this->get_default_taxonomy_args(),
            $this->get_additional_taxonomy_args()
        );
    }
    
}


class RealEstateRegistration extends RegisterNewPostType{
    protected function get_post_type_name() {
        return 'real-estate';
    }
    
    protected function get_additional_args(){
        return array(
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => 'real-estate'),
            'menu_position' => 5,  
            'supports'      => array('title', 'editor', 'thumbnail', 'custom-fields'),
            'menu_icon'     => 'dashicons-building', 
            'show_in_rest'=> true,
        );
    }
    
    protected function get_taxonomy_name() {
        return 'real-estate-type';
    }

    protected function get_additional_taxonomy_args(){
        return array(
            'label' => 'Тип недвижимости',
            'rewrite' => array('slug' => 'real-estate-type'),
            'hierarchical' => true,
        );
    }
    public function register_taxonomy() {
        $args = array_merge(
            $this->get_default_taxonomy_args(),
            $this->get_additional_taxonomy_args()
        );
        register_taxonomy($this->get_taxonomy_name(), $this->get_post_type_name(), $args);
        add_action('add_meta_boxes', array($this, 'add_real_estate_metabox'));
        add_action('save_post', array($this, 'save_real_estate_metabox'));
    }

    public function add_real_estate_metabox(){
        add_meta_box(
            '_real_estate_city',  
            'Связанный город',  
            array($this, 'real_estate_metabox_callback'),  
            'real-estate',  
            'normal', 
            'high'  
        );
    }

    public function real_estate_metabox_callback($post) {
        $city_id = get_post_meta($post->ID, '_real_estate_city', true);
        echo '<p><label for="_real_estate_city">Выберите город:</label></p>';
        echo '<select name="_real_estate_city" id="_real_estate_city">';
        echo '<option value="0">None</option>';

        
        $city_args = array(
            'post_type' => 'city',
            'posts_per_page' => -1,
        );
        $city_posts = new WP_Query($city_args);

        while ($city_posts->have_posts()) {
            $city_posts->the_post();
            $selected = ($city_id == get_the_ID()) ? 'selected' : '';
            echo '<option value="' . get_the_ID() . '" ' . $selected . '>' . get_the_title() . '</option>';
        }
        echo '</select>';
        wp_reset_postdata();
    }
    public function save_real_estate_metabox($post_id) {
        if (isset($_POST['_real_estate_city'])) {
            update_post_meta($post_id, '_real_estate_city', sanitize_text_field($_POST['_real_estate_city']));
        }
    }
}

class CityRegistration extends RegisterNewPostType {
    protected function get_taxonomy_name() {
        return '';
    }
    protected function get_additional_taxonomy_args(){
        return array();
    }
    protected function get_post_type_name(){
        return 'city';
    }

    protected function get_additional_args(){
        return array(
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => 'cities'),
            'menu_position' => 5,  
            'supports'      => array('title', 'editor', 'thumbnail', 'custom-fields'),
            'menu_icon'     => 'dashicons-location', 
            'show_in_rest'=> true,
        );
    }
}


$real_estate_labels = array(
    'name'               => 'Недвижимость ',
    'singular_name'      => 'Объект Недвижимость',
    'add_new'            => 'Добавить новый',
    'add_new_item'       => 'Добавить новый объект ',
    'edit_item'          => 'Редактировать объект',
    'new_item'           => 'Новый объект',
    'all_items'          => 'Объекты ',
    'view_item'          => 'Посмотреть объект ',
    'search_items'       => 'Поиск объекта ',
    'not_found'          => 'Ничего не найдено ',
    'not_found_in_trash' => 'Ничего не найдено ',
    'parent_item_colon'  => '',
    'menu_name'          => 'Недвижимость'
);
$city_labels = array(
    'name'               => 'Города',
    'singular_name'      => 'Город',
    'add_new'            => 'Добавить новый город',
    'add_new_item'       => 'Добавить новый город ',
    'edit_item'          => 'Редактировать город',
    'new_item'           => 'Новый город',
    'all_items'          => 'Города ',
    'view_item'          => 'Посмотреть город ',
    'search_items'       => 'Поиск город ',
    'not_found'          => 'Ничего не найдено ',
    'not_found_in_trash' => 'Ничего не найдено ',
    'parent_item_colon'  => '',
    'menu_name'          => 'Города'
);
$real_estate_taxonomy_labels = array(
    'label' => 'Тип недвижимости',
    'rewrite' => array('slug' => 'real-estate-type'),
    'hierarchical' => true,
);
$city_taxonomy_labels = array();
$real_estate_post_type = new RealEstateRegistration($real_estate_labels, $real_estate_taxonomy_labels);
$city_post_type = new CityRegistration($city_labels, $city_taxonomy_labels);