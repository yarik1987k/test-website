<?php 

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$object_gallery = get_field('object_gallery');
$square = get_field('square');
$price = get_field('price');
$address = get_field('address');
$living_space = get_field('living_space');
$floor = get_field('floor');
$terms = get_the_terms( $post->ID , 'real-estate-type' );

$allowed_tags = array(
    'img' => array(
        'src' => true,
        'alt' => true,
        'width' => true,
        'height' => true,
    ),
);
?>
<main class="single single--estate">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-5">
                <div class="single__info ">
                    <h1><?php the_title();?></h1>
                    <div class="single__info-data pt-md-5 pt-3">
                        <?php if(!empty($square)):?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="single__info-data-item pb-3">
                                        <span>Площадь:</span>
                                        <h5><?php echo esc_html($square); ?></h5>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($price)):?>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="single__info-data-item pb-3">                                    
                                <span>Стоимость:</span>
                                    <h5><?php echo esc_html($price); ?></h5>
                        </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($address)):?>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="single__info-data-item pb-3">
                                <span>Адрес:</span>
                                    <h5><?php echo esc_html($address); ?></h5>
                                </div>
                                </div>
                            </div>
                        <?php endif;?>       
                        <?php if(!empty($living_space)):?>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="single__info-data-item pb-3">
                                <span>Жилая площадь:</span>
                                    <h5><?php echo esc_html($living_space); ?></h5>
                                </div>
                                </div>
                            </div>
                        <?php endif;?>       
                        <?php if(!empty($floor)):?>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="single__info-data-item pb-3">
                                <span>Этаж:</span>
                                    <h5><?php echo esc_html($floor); ?></h5>
                                </div>
                                </div>
                            </div>
                        <?php endif;?>           
                        <?php if(!empty($terms)):?>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="single__info-data-item pb-3">
                                <span>Тип объекта :</span>
                                <?php 
                                    foreach($terms as $term){
                                        echo '<h5>'.$term->name.'</h5>';
                                    }
                                ?> 
                                </div>
                                </div>
                            </div>
                        <?php endif;?>                                                                                                         
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-7">
                <div class="single__slider slider-block pt-md-5">
                    <?php 
                        if($object_gallery){
                            
                            foreach($object_gallery as $image){
                                echo wp_kses(
                                    wp_get_attachment_image($image['id'], 'full'),
                                    $allowed_tags
                                );
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="single__content pt-5">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="pb-3">Описание </h2>
                    <?php the_content();?>                
                </div> 
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
