<?php
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$city_id = get_the_ID();
$args = array(
    'post_type' => 'real-estate',
    'meta_key' => '_real_estate_city',
    'meta_value' => $city_id,
    'posts_per_page' => -1,
);
$properties_query = new WP_Query($args);

?>

<main class="single single--city">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-5">
                <div class="single__info ">
                    <h1><?php the_title();?></h1>
                    <div class="single__content pt-5">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="pb-3">Описание </h2>
                                <?php the_content();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-7">
                <div class="single__slider pt-md-5">
                    <?php 
                        if (has_post_thumbnail()) {
                            echo '<div class="city-featured-image">';
                            the_post_thumbnail('full');
                            echo '</div>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <section class="block-grid py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="pb-5">Объекты в этом городе:</h2>
                </div>                
                <?php
                    if ($properties_query->have_posts()) {
                        
                        
                        while ($properties_query->have_posts()) {
                            $properties_query->the_post();
                            $square = get_field('square', get_the_ID());
                            $price = get_field('price', get_the_ID()); 
                            $living_space = get_field('living_space', get_the_ID());
                            $floor = get_field('floor', get_the_ID()); 
                            ?>
                
                <div class="col-md-3 mb-5">
                    <div class="block-grid__single">
                        <a href="<?php echo get_permalink(); ?>">
                            <figure>
                                <?php 
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail('full');   
                                    }
                                ?>
                            </figure>
                        </a>
                        <a href="<?php echo get_permalink(); ?>" class="d-flex justify-content-center p-3">
                            <h6><?php the_title(); ?> <h6>
                        </a>
                        <ul class="pb-3">
                            <?php if(!empty($floor)):?><li><span>Этаж:</span> - <?php echo esc_html($floor); ?></li><?php endif;?>
                            <?php if(!empty($living_space)):?><li><span>Жилая площадь:</span> - <?php echo esc_html($living_space); ?></li><?php endif;?>
                            <?php if(!empty($price)):?><li><span>Стоимость:</span> - <?php echo esc_html($price); ?></li><?php endif;?>
                            <?php if(!empty($square)):?><li><span>Площадь:</span> - <?php echo esc_html($square); ?></li><?php endif;?>
                        </ul>
                    </div>
                </div>
                <?php }
                        
                        wp_reset_postdata();
                    } else {
                        echo 'No properties found.';
                    }
                ?>
            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>