// Add your custom JS here.
var $j = jQuery.noConflict();

$j(document).ready(function() {
    const slider = document.querySelector('.slider-block');

    if (slider) {
        $j(slider).slick({
			dots: true,
			infinite: true,
			speed: 500,
			fade: true,
			cssEase: 'linear',
			autoplay: true,
			autoplaySpeed: 2000,
			arrows: true,
        });
    }

    function isValidForm() {
        var propertyName = $j('#property-name'); 
        if (propertyName.val().trim() === '') {
            $j(propertyName).addClass('error');
            return false;
        }else{
            $j(propertyName).removeClass('error');
        } 
      
        return true;  
    }

    $j('#image-upload-form').on('submit', function(e) {
        e.preventDefault();
    
        if (!isValidForm()) {
            console.log('error');
            return;  
        }
        let images = $j('#images');
        var formData = new FormData(this);
     
        formData.append('property-name', $j('#property-name').val());
        formData.append('property-description', $j('#property-description').val());
        formData.append('property-square', $j('#property-square').val());
        formData.append('property-price', $j('#property-price').val());
        formData.append('property-address', $j('#property-address').val());
        formData.append('property-living-space', $j('#property-living-space').val());
        formData.append('property-floor', $j('#property-floor').val()); 
        formData.append('property-city', $j('#property-city').val()); 
        formData.append('property-type', $j('#property-type').val()); 
    
        formData.append('action', 'upload_images'); 
        if(images){ 
            $j.each($j(images), function(i, obj) {
                $j.each(obj.files,function(j,file){
                    formData.append('files[' + j + ']', file);
                }) 
            });
        } 
 
        $j.ajax({
            url: ajaxurl,
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: formData,
            success: function(response) {
                $j("#upload-status").html(response.message);
            } 
        });
    });
    
    
    
});
 