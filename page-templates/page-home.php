<?php
/**
 * Template Name: Home Page
 *
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

while ( have_posts() ) :
	the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
 <section class = "inner-page-wrapper">
        <section class = "container">
 
            </section>
        </section>
    </section>
<?php
get_footer();
